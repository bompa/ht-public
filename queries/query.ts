import { getPropertyName } from '../names/propname'

export class QueryT<SelectT, FilterT> {
  public static CURRENT_USER_VALUE: string = '$current_user';

  public select: SelectT;
  public filter: FilterT;

  /**
   * HTTP param strings reflecting select/filter values. 
   */
  public params: {
    select?: any;
    filter?: any;
  }
}

export class Query extends QueryT<any, any> {
}

/**
 * Convert filter param to string used in http request
 */
export function toQueryParamFilter(query: any, property: any): string {
  let param = getPropertyName(query, property);
  if (param) {
    return 'where_' + param.toLowerCase();
  }
  return '';
}

/**
 * Get full HTTP query string from query object, e.g:
 * ?param=x&where_filter=y
 * @param query 
 */
export function getQueryParams(query: QueryT<any, any>): string {
  if (query && query.filter) {
    let params = '';
    if (query.filter.userID) {
      params += toQueryParamFilter(query, query.filter.userID) + '=' + query.filter.userID;
    }

    if (params) {
      return '?' + params;
    }
  }

  return '';
}