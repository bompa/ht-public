export declare class QueryT<SelectT, FilterT> {
    static CURRENT_USER_VALUE: string;
    select: SelectT;
    filter: FilterT;
    params: {
        select?: any;
        filter?: any;
    };
}
export declare class Query extends QueryT<any, any> {
}
export declare function toQueryParamFilter(query: any, property: any): string;
export declare function getQueryParams(query: QueryT<any, any>): string;
