"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var query_1 = require("./query");
var RealEstatesFilter = (function () {
    function RealEstatesFilter() {
        this.userID = '';
    }
    return RealEstatesFilter;
}());
exports.RealEstatesFilter = RealEstatesFilter;
var RealEstatesQuery = (function (_super) {
    __extends(RealEstatesQuery, _super);
    function RealEstatesQuery() {
        var _this = _super.call(this) || this;
        _this.filter = new RealEstatesFilter();
        _this.params = {
            filter: {
                userID: query_1.toQueryParamFilter(_this.filter, _this.filter.userID)
            }
        };
        return _this;
    }
    RealEstatesQuery.create = function () {
        return new RealEstatesQuery();
    };
    return RealEstatesQuery;
}(query_1.QueryT));
exports.RealEstatesQuery = RealEstatesQuery;
//# sourceMappingURL=realestatesquery.js.map