import { QueryT } from './query';
export declare class RealEstatesFilter {
    userID: string;
}
export declare class RealEstatesQuery extends QueryT<any, RealEstatesFilter> {
    static create(): RealEstatesQuery;
    constructor();
}
