import { QueryT, toQueryParamFilter } from './query'

export class RealEstatesFilter {
  public userID: string = '';
}

export class RealEstatesQuery extends QueryT<any, RealEstatesFilter> {
  public static create() {
    return new RealEstatesQuery();
  }

  constructor() {
    super();

    this.filter = new RealEstatesFilter();    
    this.params = {
      filter: {
        userID: toQueryParamFilter(this.filter, this.filter.userID)
      }
    }
  }
}
