"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var propname_1 = require("../names/propname");
var QueryT = (function () {
    function QueryT() {
    }
    return QueryT;
}());
QueryT.CURRENT_USER_VALUE = '$current_user';
exports.QueryT = QueryT;
var Query = (function (_super) {
    __extends(Query, _super);
    function Query() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Query;
}(QueryT));
exports.Query = Query;
function toQueryParamFilter(query, property) {
    var param = propname_1.getPropertyName(query, property);
    if (param) {
        return 'where_' + param.toLowerCase();
    }
    return '';
}
exports.toQueryParamFilter = toQueryParamFilter;
function getQueryParams(query) {
    if (query && query.filter) {
        var params = '';
        if (query.filter.userID) {
            params += toQueryParamFilter(query, query.filter.userID) + '=' + query.filter.userID;
        }
        if (params) {
            return '?' + params;
        }
    }
    return '';
}
exports.getQueryParams = getQueryParams;
//# sourceMappingURL=query.js.map