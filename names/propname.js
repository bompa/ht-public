"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function propName(prop, value, res) {
    for (var i in prop) {
        if (typeof prop[i] == 'object') {
            if (res = propName(prop[i], value, res)) {
                return res;
            }
        }
        else {
            if (prop[i] == value) {
                res = i;
                return res;
            }
        }
    }
    return undefined;
}
function getPropertyName(object, property) {
    var res = '';
    return propName(object, property, res);
}
exports.getPropertyName = getPropertyName;
//# sourceMappingURL=propname.js.map