function propName(prop: any, value: any, res: any)
{
   for(var i in prop)
   {
       if ( typeof prop[i] == 'object'  )
       {
         if ( res = propName(prop[i], value, res) )
         {
            return res;
         }
       }
       else
       {
         if (prop[i] == value)
         {
              res = i;
              return res;
         }
       }
   }
   return undefined;
}

/**
 * 
 * @param object 
 * @param property 
 */
export function getPropertyName(object: any, property: any) {
  let res = '';
  return propName(object, property, res);
}