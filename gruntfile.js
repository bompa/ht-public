module.exports = function (grunt) {
  "use strict";

  grunt.initConfig({
    copy: {
      build: {
        files: [
          {
            expand: true,
            cwd: "./queries",
            src: ["**"],
            dest: "../hemtorg-server/node_modules/ht-public/queries"
          },
          {
            expand: true,
            cwd: "./queries",
            src: ["**"],
            dest: "../hemtorg-site/node_modules/ht-public/queries"
          },
          {
            expand: true,
            cwd: ".",
            src: ["*.ts", "*.js"],
            dest: "../hemtorg-server/node_modules/ht-public"
          },
          {
            expand: true,
            cwd: ".",
            src: ["*.ts", "*.js"],
            dest: "../hemtorg-site/node_modules/ht-public"
          },
          {
            expand: true,
            cwd: "./names",
            src: ["*.ts", "*.js"],
            dest: "../hemtorg-server/node_modules/ht-public/names"
          },
          {
            expand: true,
            cwd: "./names",
            src: ["*.ts", "*.js"],
            dest: "../hemtorg-site/node_modules/ht-public/names"
          },
          {
            expand: true,
            cwd: "./interfaces",
            src: ["*.ts", "*.js"],
            dest: "../hemtorg-server/node_modules/ht-public/interfaces"
          },
          {
            expand: true,
            cwd: "./interfaces",
            src: ["*.ts", "*.js"],
            dest: "../hemtorg-site/node_modules/ht-public/interfaces"
          }
        ]
      }
    },
    ts: {
      app: {
        tsconfig: true
      }
    },
    watch: {
      ts: {
        files: ["interfaces/\*\*/\*.ts", "queries/\*\*/\*.ts", "names/\*\*/\*.ts", "\*.ts"],
        tasks: ["ts", "copy"]
      }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-ts");

  grunt.registerTask('default', ['ts', 'copy', 'watch']);
};