import { GeoLocation } from './geolocation.interface';
import { PriceRange } from './pricerange.interface';
export interface RealEstateProperties {
    address: string;
    postcode: string;
    type: number;
    size: string;
    priceRange: PriceRange;
    monthlyFee: number;
    city: string;
    owner: string;
    placeID?: string;
    loc: GeoLocation;
    images?: any[];
    isPrivate: boolean;
}
