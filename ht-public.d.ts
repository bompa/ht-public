import { Query } from './queries/query';
import { RealEstatesQuery } from './queries/realestatesquery';
declare const _default: {
    Query: typeof Query;
    RealEstatesQuery: typeof RealEstatesQuery;
    getPropertyName: (object: any, property: any) => any;
};
export = _default;
