"use strict";
var query_1 = require("./queries/query");
var realestatesquery_1 = require("./queries/realestatesquery");
var propname_1 = require("./names/propname");
module.exports = { Query: query_1.Query, RealEstatesQuery: realestatesquery_1.RealEstatesQuery, getPropertyName: propname_1.getPropertyName };
//# sourceMappingURL=ht-public.js.map