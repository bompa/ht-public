import { Query } from './queries/query'
import { RealEstatesQuery } from './queries/realestatesquery'
import { RealEstateProperties } from './interfaces/realestateproperties.interface'
import { getPropertyName } from './names/propname'

export = { Query, RealEstatesQuery, getPropertyName }
